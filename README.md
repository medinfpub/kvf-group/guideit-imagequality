# guideit-imagequality

jupyter notebook and sample files for the experiments conducted for the article

Quality Assessment for Secondary Use of Imaging Trials

accepted as Full paper on the [MIE 2024](https://mie2024.org/)

The submitted version of the paper is found in this repository

The doi of the published version follows as soon as possible

